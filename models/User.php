<?php

namespace app\models;

use app\traits\UserIdentity;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int         $id
 * @property string      $username
 * @property string      $password
 * @property string      $first_name
 * @property string      $last_name
 * @property string      $description
 * @property string      $avatar
 * @property string      $access_token
 * @property string      $auth_key
 *
 * @property Dashboard[] $dashboards
 */
class User extends ActiveRecord implements IdentityInterface
{
    use UserIdentity;

    public const AVATAR_PATH = 'img/avatars';

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'users';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['username', 'password'], 'required'],
            [['description'], 'string'],
            [['username'], 'string', 'max' => 32],
            [['password', 'first_name', 'last_name', 'access_token', 'auth_key'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['!password'], 'safe'],
            [['avatar'], 'file', 'extensions' => ['jpg', 'jpeg', 'png']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'          => 'ID',
            'username'    => 'Логин',
            'password'    => 'Пароль',
            'first_name'  => 'Имя',
            'last_name'   => 'Фамилия',
            'description' => 'Обо мне',
            'avatar'      => 'Аватар',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getDashboards(): ActiveQuery
    {
        return $this->hasMany(Dashboard::class, ['owner_id' => 'id']);
    }

    /**
     * @param string $password
     *
     * @throws Exception
     */
    public function setPassword(string $password): void
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
}
