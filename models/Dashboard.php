<?php

namespace app\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dashboard".
 *
 * @property int    $id
 * @property int    $owner_id
 * @property string $title
 * @property string $image
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User   $owner
 */
class Dashboard extends ActiveRecord
{
    public const IMAGE_PATH = 'img/dashboard';

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'dashboard';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['title', 'image', 'description'], 'required'],
            [['owner_id'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['owner_id' => 'id']],
            [['image'], 'file', 'extensions' => ['jpg', 'jpeg', 'png']],
        ];
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class'              => BlameableBehavior::class,
                'createdByAttribute' => 'owner_id',
                'updatedByAttribute' => false,
            ],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'          => 'ID',
            'owner_id'    => 'Создал',
            'title'       => 'Заголовок',
            'image'       => 'Изображение',
            'description' => 'Описание',
            'created_at'  => 'Дата создания',
            'updated_at'  => 'Дата изменения',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOwner(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'owner_id']);
    }
}
