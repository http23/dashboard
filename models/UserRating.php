<?php

namespace app\models;

use yii\behaviors\BlameableBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_rating".
 *
 * @property int  $id
 * @property int  $owner_id
 * @property int  $user_id
 * @property int  $rating
 *
 * @property User $owner
 * @property User $user
 */
class UserRating extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'user_rating';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['user_id', 'rating'], 'required'],
            [['owner_id', 'user_id', 'rating'], 'integer'],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['owner_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['rating'], 'number', 'min' => 1, 'max' => 5],
        ];
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'blameable' => [
                'class'              => BlameableBehavior::class,
                'createdByAttribute' => 'owner_id',
                'updatedByAttribute' => false,
            ],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'       => 'ID',
            'owner_id' => 'Owner ID',
            'user_id'  => 'User ID',
            'rating'   => 'Рейтинг',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOwner(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'owner_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
