<?php

namespace app\helpers;


use Yii;
use yii\db\ActiveRecord;

class FlashHelper
{
    public function __construct(ActiveRecord $model, $successText = 'Данные сохранены')
    {
        $this->_model       = $model;
        $this->_successText = $successText;
    }

    /**
     * @var ActiveRecord
     */
    private $_model;
    /**
     * @var string
     */
    private $_successText;

    public function save($attributeNames = null): bool
    {
        if ($this->_model->save(true, $attributeNames)) {
            Yii::$app->session->addFlash('success', $this->_successText);

            return true;
        }

        foreach ($this->_model->errors as $error) {
            Yii::$app->session->addFlash('error', reset($error));
        }

        return false;
    }
}
