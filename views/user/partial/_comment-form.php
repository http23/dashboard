<?php
/* @var $model UserComment */

/* @var $userID int */

use app\models\UserComment;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$form = ActiveForm::begin([
    'action'      => "/user/{$userID}/add-comment",
    'layout'      => 'horizontal',
    'fieldConfig' => [
        'template' => '<br><div class="col-lg-12">{input}</div><br><div class="col-lg-12">{error}</div>',
    ],
]);

echo $form->field($model, 'text')->textarea();
?>

<div class="form-group">
    <div class="col-lg-12 text-right">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
