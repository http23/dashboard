<?php
/* @var $model UserComment */

use app\models\UserComment;
use yii\helpers\Html;

echo Html::a($model->owner->username, '/user/' . $model->owner_id, ['title' => (new DateTime($model->created_at))->format('H:i d.m.Y')]) . ': ' . $model->text;

