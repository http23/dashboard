<?php

/* @var $this yii\web\View */

/* @var $model \app\models\User */

use dosamigos\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin([
    'id'          => 'login-form',
    'layout'      => 'horizontal',
    'fieldConfig' => [
        'template'     => "{label}\n<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
    'options'     => ['enctype' => 'multipart/form-data'],
]);

echo $form->field($model, 'first_name')->textInput();
echo $form->field($model, 'last_name')->textInput();
echo $form->field($model, 'description')->widget(CKEditor::class, ['preset' => 'basic']);
echo $form->field($model, 'avatar')->fileInput();
?>

<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
