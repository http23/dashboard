<?php

/* @var $this yii\web\View */

use app\models\User;
use app\models\UserComment;
use app\widgets\UserRating;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\i18n\Formatter;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $model User */
/* @var $isOwner boolean */
/* @var $dataProvider ActiveDataProvider */
/* @var $userCommentModel UserComment */

$this->params['breadcrumbs'][] = $this->title;

$widgetAttributes = [
    [
        'label'  => 'Аватар',
        'value'  => $model->avatar ? Html::img('/' . User::AVATAR_PATH . '/' . $model->avatar, ['class' => 'col-md-6']) : 'Отсутствует',
        'format' => 'raw',
    ],
    'first_name',
    'last_name',
    'description:raw',
];

if (!$isOwner) {
    $widgetAttributes[] = [
        'label'  => 'Рейтинг',
        'value'  => UserRating::widget([
            'name'    => 'user-rating',
            'userID'  => $model->id,
            'options' => [
                'id' => 'star-widget',
            ],
        ]),
        'format' => 'raw',
    ];
}

echo DetailView::widget([
    'model'      => $model,
    'formatter'  => new Formatter([
        'nullDisplay' => 'Не указано',
    ]),
    'attributes' => $widgetAttributes,
]);

if ($isOwner) {
    echo Html::a('Редактировать', '/user/edit', ['class' => 'btn btn-block btn-warning']);
}
?>

<hr>
<h3>Комментарии</h3>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => '<br>{items}<br>{pager}',
    'itemView'     => 'partial/_comment',
    'separator'    => '<hr>',
    'emptyText'    => 'Комментариев пока нет...',
]);

if (!Yii::$app->user->isGuest) {
    echo $this->render('partial/_comment-form', ['model' => $userCommentModel, 'userID' => $model->id]);
}
?>
