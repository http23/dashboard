<?php

/**
 * @var ActiveDataProvider $dataProvider
 * @var View               $this
 */

use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\widgets\ListView;

$this->registerCss(<<<CSS
    .dashboard-widget .image {
        float: left;
        overflow: hidden;
        width: 200px;
        height: 200px;
        margin: 0 20px 10px 0;
    }
    .dashboard-widget .image img {
        width: auto;
        height: 100%;
    }
    .dashboard-widget .content {
        margin-left: 200px;
    }
    .overflowed {
        overflow: hidden;
    }
    .dashboard-widget .title {
        margin: 0 0 15px 0;
    }
    .dashboard-widget .date {
        color:#9d9d9d;
    }
CSS
);

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'     => 'partial/_entity',
    'separator'    => '<hr>',
    'layout'       => '{items}<br>{pager}',
    'emptyText'    => 'Объявления отсутствуют',
    'options'      => ['class' => 'dashboard-widget'],
    'itemOptions'  => ['class' => 'overflowed'],
]);
