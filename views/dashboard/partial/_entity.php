<?php
/* @var $model Dashboard */

use app\models\Dashboard;
use yii\helpers\Html;
use yii\helpers\StringHelper;

?>

<div class="image">
    <?= Html::a(Html::img('/' . $model::IMAGE_PATH . '/' . $model->image), '/dashboard/' . $model->id) ?>
</div>
<div class="content">
    <div class="title"><?= Html::a($model->title, '/dashboard/' . $model->id) ?></div>
    <div class="short-text"><?= StringHelper::truncateWords($model->description, 20) ?></div>
    <div class="date"><?= (new DateTime($model->created_at))->format('d.m.Y') ?></div>
</div>

