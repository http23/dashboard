<?php

/**
 * @var Dashboard $model
 * @var View      $this
 */

use app\models\Dashboard;
use yii\bootstrap\Html;
use yii\web\View;

$this->params['breadcrumbs'][] = $this->title;

$this->registerCss(<<<CSS
    .db-poster {
        max-width: 400px;
        max-height: 400px;
    }
CSS
);
?>
<div class="text-center"><?= Html::img('/' . $model::IMAGE_PATH . '/' . $model->image, ['class' => 'db-poster']) ?></div>
<hr>
<?= $model->description ?>
<hr>
<b>Опубликовал: </b><?= Html::a($model->owner->username, '/user/' . $model->owner_id) ?> (<?= (new DateTime($model->created_at))->format('H:i d.m.Y') ?>)
