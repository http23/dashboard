<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 02.09.2018
 * Time: 0:01
 */

namespace app\widgets;


use app\models\UserRating as RatingModel;
use Yii;
use yii\web\JsExpression;
use yii2mod\rating\StarRating;

class UserRating extends StarRating
{
    public $userID;

    public function init()
    {
        $this->value         = round(RatingModel::find()->where(['user_id' => $this->userID])->average('rating'), 1);
        $this->clientOptions = [
            'cancel' => $this->hasRating(),
            'click'  => new JsExpression('function(score, e) {
			            $.post(\'/user/set-rating\', {rating: score, userID: ' . $this->userID . '}, function() {
			                location.reload();
                        });
		            }'),
        ];

        parent::init();
    }

    private function hasRating(): bool
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        return RatingModel::find()->where(['user_id' => $this->userID, 'owner_id' => Yii::$app->user->id])->exists();
    }
}
