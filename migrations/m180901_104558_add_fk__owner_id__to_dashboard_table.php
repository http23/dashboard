<?php

use yii\db\Migration;

/**
 * Class m180901_104558_add_fk__owner_id__to_dashboard_table
 */
class m180901_104558_add_fk__owner_id__to_dashboard_table extends Migration
{
    private const FK_NAME = 'fk_$id_$owner_id__dashboard';

    private const USER_TN      = 'users';
    private const DASHBOARD_TN = 'dashboard';

    public function safeUp()
    {
        $this->addForeignKey(
            self::FK_NAME,
            self::DASHBOARD_TN,
            'owner_id',
            self::USER_TN,
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_NAME, self::DASHBOARD_TN);
    }
}
