<?php

use yii\db\Migration;

/**
 * Class m180901_104303_create_table_dashboard
 */
class m180901_104303_create_table_dashboard extends Migration
{
    private const TABLE_NAME = 'dashboard';

    public function safeUp()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id'          => $this->primaryKey()->unsigned(),
                'owner_id'    => $this->integer()->notNull()->unsigned(),
                'title'       => $this->string(255)->notNull(),
                'image'       => $this->string(255)->notNull(),
                'description' => $this->text()->notNull(),
                'created_at'  => $this->dateTime()->notNull(),
                'updated_at'  => $this->dateTime()->notNull(),
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
