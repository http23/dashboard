<?php

use yii\db\Migration;

/**
 * Class m180901_101155_create_table_user_comments
 */
class m180901_101155_create_table_user_comments extends Migration
{
    private const TABLE_NAME = 'user_comments';

    public function safeUp()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id'         => $this->primaryKey()->unsigned(),
                'owner_id'   => $this->integer()->unsigned()->notNull(),
                'user_id'    => $this->integer()->unsigned()->notNull(),
                'text'       => $this->string(255)->notNull(),
                'created_at' => $this->dateTime()->notNull(),
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
