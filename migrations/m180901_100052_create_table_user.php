<?php

use yii\db\Migration;

/**
 * Class m180901_100052_create_table_user
 */
class m180901_100052_create_table_user extends Migration
{
    private const TABLE_NAME = 'users';

    public function safeUp()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id'           => $this->primaryKey()->unsigned(),
                'username'     => $this->string(32)->notNull()->unique(),
                'password'     => $this->string(255)->notNull(),
                'first_name'   => $this->string(255),
                'last_name'    => $this->string(255),
                'description'  => $this->text(),
                'avatar'       => $this->string(255),
                'access_token' => $this->string(255),
                'auth_key'     => $this->string(255),
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
