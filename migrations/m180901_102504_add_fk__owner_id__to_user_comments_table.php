<?php

use yii\db\Migration;

/**
 * Class m180901_102504_add_fk__owner_id__to_user_comments_table
 */
class m180901_102504_add_fk__owner_id__to_user_comments_table extends Migration
{
    private const FK_NAME = 'fk_$id_$owner_id__comments';

    private const USER_TN          = 'users';
    private const USER_COMMENTS_TN = 'user_comments';

    public function safeUp()
    {
        $this->addForeignKey(
            self::FK_NAME,
            self::USER_COMMENTS_TN,
            'owner_id',
            self::USER_TN,
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_NAME, self::USER_COMMENTS_TN);
    }
}
