<?php

use yii\db\Migration;

/**
 * Class m180901_103247_add_fk__owner_id__to_user_rating_table
 */
class m180901_103247_add_fk__owner_id__to_user_rating_table extends Migration
{
    private const FK_NAME = 'fk_$id_$owner_id__rating';

    private const USER_TN        = 'users';
    private const USER_RATING_TN = 'user_rating';

    public function safeUp()
    {
        $this->addForeignKey(
            self::FK_NAME,
            self::USER_RATING_TN,
            'owner_id',
            self::USER_TN,
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_NAME, self::USER_RATING_TN);
    }
}
