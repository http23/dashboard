<?php

use yii\db\Migration;

/**
 * Class m180901_101743_create_table_user_rating
 */
class m180901_101743_create_table_user_rating extends Migration
{
    private const TABLE_NAME = 'user_rating';

    public function safeUp()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id'       => $this->primaryKey()->unsigned(),
                'owner_id' => $this->integer()->unsigned()->notNull(),
                'user_id'  => $this->integer()->unsigned()->notNull(),
                'rating'   => $this->tinyInteger(1)->unsigned()->notNull(),
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
