<?php

namespace app\controllers;

use app\actions\dashboard\Add;
use app\actions\dashboard\Index;
use app\actions\dashboard\View;
use yii\filters\AccessControl;
use yii\web\Controller;

class DashboardController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only'  => ['add'],
                'rules' => [
                    [
                        'actions' => ['add'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'index' => Index::class,
            'view'  => View::class,
            'add'   => Add::class,
        ];
    }
}
