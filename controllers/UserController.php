<?php

namespace app\controllers;

use app\actions\user\AddComment;
use app\actions\user\Edit;
use app\actions\user\ProfilePage;
use app\actions\user\SetRating;
use app\actions\user\SignIn;
use app\actions\user\SignOut;
use app\actions\user\SignUp;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class UserController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only'  => ['sign-out', 'edit', 'set-rating', 'add-comment'],
                'rules' => [
                    [
                        'actions' => ['sign-out', 'edit', 'set-rating', 'add-comment'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'sign-out'    => ['post'],
                    'set-rating'  => ['post'],
                    'add-comment' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'sign-in'      => SignIn::class,
            'sign-up'      => SignUp::class,
            'sign-out'     => SignOut::class,
            'edit'         => Edit::class,
            'profile-page' => ProfilePage::class,
            'set-rating'   => SetRating::class,
            'add-comment'  => AddComment::class,
        ];
    }
}
