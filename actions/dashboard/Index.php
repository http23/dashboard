<?php

namespace app\actions\dashboard;

use app\models\Dashboard;
use yii\base\Action;
use yii\data\ActiveDataProvider;

class Index extends Action
{
    public function run()
    {
        $this->controller->view->title = 'Главная страница';

        $dataProvider = new ActiveDataProvider([
            'query'      => Dashboard::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort'       => [
                'defaultOrder' => ['created_at' => SORT_DESC],
            ],
        ]);

        return $this->controller->render('index', ['dataProvider' => $dataProvider]);
    }
}
