<?php

namespace app\actions\dashboard;

use app\helpers\FlashHelper;
use app\models\Dashboard;
use DateTime;
use Yii;
use yii\base\Action;
use yii\base\InvalidArgumentException;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class Add extends Action
{
    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function run()
    {
        $this->controller->view->title = 'Добавление объявления';

        $model = new Dashboard();

        if ($data = Yii::$app->request->post('Dashboard')) {
            $model->load($data, '');
            $this->uploadImage($model);

            if ((new FlashHelper($model, 'Данные опубликованы'))->save()) {
                $this->controller->redirect('/dashboard/' . $model->id);
            } else {
                FileHelper::unlink(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $model::IMAGE_PATH . DIRECTORY_SEPARATOR . $model->image);
            }
        }

        return $this->controller->render('add', ['model' => $model]);
    }

    private function uploadImage(Dashboard $model)
    {
        $model->image = UploadedFile::getInstance($model, 'image');

        if ($model->image && $model->validate()) {
            $fileName = md5((new DateTime())->getTimestamp() . $model->image->baseName) . '.' . $model->image->extension;

            $model->image->saveAs(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $model::IMAGE_PATH . DIRECTORY_SEPARATOR . $fileName);
            $model->image = $fileName;

            return true;
        }

        return false;
    }
}
