<?php

namespace app\actions\dashboard;

use app\models\Dashboard;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class View extends Action
{
    public function run(int $id)
    {
        $model = Dashboard::find()->with('owner')->where(['id' => $id])->limit(1)->one();

        if (null === $model) {
            throw new NotFoundHttpException("Объявление с ID: {$id} не найдено");
        }

        $this->controller->view->title = $model->title;

        return $this->controller->render('view', ['model' => $model]);
    }
}
