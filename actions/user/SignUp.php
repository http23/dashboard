<?php

namespace app\actions\user;

use app\models\User;
use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class SignUp extends Action
{
    /**
     * @return string|Response
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function run()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->controller->goHome();
        }

        $this->controller->view->title = 'Регистрация';

        $model = new User();

        if ($data = Yii::$app->request->post('User')) {
            $model->load($data, '');

            $model->setPassword(ArrayHelper::getValue($data, 'password'));

            if ($model->save()) {
                Yii::$app->user->login($model);

                $this->controller->goHome();
            }
        }

        return $this->controller->render('sign-up', [
            'model' => $model,
        ]);
    }
}
