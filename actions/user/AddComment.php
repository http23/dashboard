<?php

namespace app\actions\user;

use app\helpers\FlashHelper;
use app\models\UserComment;
use Yii;
use yii\base\Action;

class AddComment extends Action
{
    public function run(int $userID)
    {
        $comment = new UserComment();

        $comment->load(Yii::$app->request->post());
        $comment->user_id = $userID;

        (new FlashHelper($comment, 'Комментарий добавлен'))->save();

        $this->controller->redirect('/user/' . $userID);
    }
}
