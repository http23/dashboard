<?php

namespace app\actions\user;

use app\models\User;
use app\models\UserComment;
use Yii;
use yii\base\Action;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class ProfilePage extends Action
{
    /**
     * @param int $userID
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function run(int $userID)
    {
        $user = User::find()->where(['id' => $userID])->limit(1)->one();

        if (null === $user) {
            throw new NotFoundHttpException("Пользователь с ID: {$userID} не найден");
        }

        $this->controller->view->title = $user->username;

        return $this->controller->render('view-profile', [
            'model'            => $user,
            'isOwner'          => $userID === Yii::$app->user->id,
            'dataProvider'     => new ActiveDataProvider([
                'query'      => UserComment::find()->with('owner')->where(['user_id' => $userID]),
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort'       => [
                    'defaultOrder' => ['created_at' => SORT_DESC],
                ],
            ]),
            'userCommentModel' => new UserComment(),
        ]);
    }
}
