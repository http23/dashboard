<?php

namespace app\actions\user;

use Yii;
use yii\base\Action;

class SignOut extends Action
{
    public function run()
    {
        Yii::$app->user->logout();

        return $this->controller->goHome();
    }
}
