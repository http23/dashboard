<?php

namespace app\actions\user;

use app\helpers\FlashHelper;
use app\models\User;
use app\models\UserRating;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SetRating extends Action
{
    /**
     * @throws \yii\db\StaleObjectException
     * @throws \Exception
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $user = User::find()->where(['id' => Yii::$app->request->post('userID')])->limit(1)->one();

        if (null === $user) {
            throw new NotFoundHttpException('Пользователь не найден');
        }

        /** @var User $owner */
        $owner = Yii::$app->user->identity;

        $ratingModel = UserRating::findOne([
            'user_id'  => $user->id,
            'owner_id' => $owner->id,
        ]);

        $rating = (int)Yii::$app->request->post('rating');

        if (null === $ratingModel) {
            $ratingModel = new UserRating();

            $ratingModel->user_id = $user->id;
        } elseif ($rating === 0) {
            return [
                'status' => $ratingModel->delete() > 0,
            ];
        }

        $ratingModel->rating = $rating;

        return [
            'status' => (new FlashHelper($ratingModel, 'Ваш голос учтен'))->save(),
        ];
    }
}
