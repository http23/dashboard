<?php

namespace app\actions\user;

use app\forms\LoginForm;
use Yii;
use yii\base\Action;

class SignIn extends Action
{
    public function run()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->controller->goHome();
        }

        $this->controller->view->title = 'Вход';

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->controller->goBack();
        }

        return $this->controller->render('sign-in', [
            'model' => $model,
        ]);
    }
}
