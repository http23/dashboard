<?php

namespace app\actions\user;

use app\helpers\FlashHelper;
use app\models\User;
use DateTime;
use Yii;
use yii\base\Action;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class Edit extends Action
{
    public function run()
    {
        $this->controller->view->title = 'Мой профиль';

        /** @var User $user */
        $user = \Yii::$app->user->identity;

        if ($data = \Yii::$app->request->post('User')) {
            $user->load($data, '');
            $this->uploadImage($user);

            if (!(new FlashHelper($user, 'Изменения сохранены'))->save()) {
                FileHelper::unlink(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $user::AVATAR_PATH . DIRECTORY_SEPARATOR . $user->avatar);
            }
        }

        return $this->controller->render('edit', ['model' => $user]);
    }

    private function uploadImage(User $model)
    {
        $model->avatar = UploadedFile::getInstance($model, 'avatar');

        if ($model->avatar && $model->validate()) {
            $fileName = md5((new DateTime())->getTimestamp() . $model->avatar->baseName) . '.' . $model->avatar->extension;

            $model->avatar->saveAs(Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $model::AVATAR_PATH . DIRECTORY_SEPARATOR . $fileName);
            $model->avatar = $fileName;

            return true;
        }

        return false;
    }
}
