<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 01.09.2018
 * Time: 14:05
 */

namespace app\traits;


use app\models\User;
use Yii;
use yii\base\Exception;
use yii\base\InvalidArgumentException;

/**
 * @mixin User
 */
trait UserIdentity
{
    /**
     * @param int $id
     *
     * @return self
     */
    public static function findIdentity($id): self
    {
        return static::findOne($id);
    }

    /**
     * @param mixed $token
     * @param null  $type
     *
     * @return self
     */
    public static function findIdentityByAccessToken($token, $type = null): self
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @param string $authKey
     *
     * @return bool
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @return null|string
     */
    public function getAuthKey(): ?string
    {
        return $this->auth_key;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $password
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    public function validatePassword(string $password): bool
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @param $insert
     *
     * @return bool
     * @throws Exception
     */
    public function beforeSave($insert): bool
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
            }

            return true;
        }

        return false;
    }
}
